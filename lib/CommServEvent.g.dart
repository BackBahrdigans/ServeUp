// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CommServEvent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommServEvent _$CommServEventFromJson(Map<String, dynamic> json) {
  return new CommServEvent(
      id: json['id'] as int,
      eventDays: (json['eventDays'] as List)
          .map((e) => new CommServEventDay.fromJson(e as Map<String, dynamic>))
          .toList(),
      name: json['name'] as String,
      description: json['description'] as String,
      parkingLocation: new Location.fromJson(
          json['parkingLocation'] as Map<String, dynamic>),
      parkingInfo: json['parkingInfo'] as String,
      leaders: (json['leaders'] as List).map((e) => e as int).toList(),
      isHidden: json['isHidden'] as bool,
      limit: json['limit'] as int,
      files: (json['files'] as List).map((e) => e as String).toList());
}

abstract class _$CommServEventSerializerMixin {
  int get id;
  String get name;
  String get description;
  List<CommServEventDay> get eventDays;
  Location get parkingLocation;
  String get parkingInfo;
  List<int> get leaders;
  int get limit;
  bool get isHidden;
  List<String> get files;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'name': name,
        'description': description,
        'eventDays': eventDays,
        'parkingLocation': parkingLocation,
        'parkingInfo': parkingInfo,
        'leaders': leaders,
        'limit': limit,
        'isHidden': isHidden,
        'files': files
      };
}
