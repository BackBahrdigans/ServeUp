// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CommServPerson.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommServPerson _$CommServPersonFromJson(Map<String, dynamic> json) {
  return new CommServPerson(
      id: json['id'] as int,
      name: json['name'] as String,
      email: json['email'] as String,
      isOfficer: json['isOfficer'] as int,
      eventsSignedUpFor: (json['eventsSignedUpFor'] as Map<String, dynamic>)
          ?.map((k, e) =>
              new MapEntry(k, (e as List)?.map((e) => e as int)?.toList())),
      eventsConfirmedFor: (json['eventsConfirmedFor'] as Map<String, dynamic>)
          ?.map((k, e) =>
              new MapEntry(k, (e as List)?.map((e) => e as int)?.toList())),
      gradYear: json['gradYear'] == null
          ? null
          : DateTime.parse(json['gradYear'] as String),
      studentID: json['studentID'] as String,
      birthday: json['birthday'] == null
          ? null
          : DateTime.parse(json['birthday'] as String),
      phone: json['phone'] as String);
}

abstract class _$CommServPersonSerializerMixin {
  String get name;
  String get email;
  int get id;
  int get isOfficer;
  Map<String, List<int>> get eventsSignedUpFor;
  Map<String, List<int>> get eventsConfirmedFor;
  DateTime get gradYear;
  String get studentID;
  DateTime get birthday;
  String get phone;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'name': name,
        'email': email,
        'id': id,
        'isOfficer': isOfficer,
        'eventsSignedUpFor': eventsSignedUpFor,
        'eventsConfirmedFor': eventsConfirmedFor,
        'gradYear': gradYear?.toIso8601String(),
        'studentID': studentID,
        'birthday': birthday?.toIso8601String(),
        'phone': phone
      };
}
