import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'BackendManager.dart';
import 'CommServPerson.dart';
import 'SelectUserView.dart';

class HelpView extends StatefulWidget {
  @override
  createState() => new HelpViewState();
  HelpView({Key key}) : super(key: key);
}

class HelpViewState extends State<HelpView> {
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text("Help"),
        ),
        body: _buildHelpView());
  }

  Widget _buildHelpView() {
    String yr = new DateFormat.y().format(new DateTime.now());
    if (yr != "2018") {
      yr = "2018-$yr";
    }
    final authors = ["Matthew Giordano", "Ishan Kamat", "Sebastian Varma"];
    authors.shuffle();
    final a1 = authors[0];
    final a2 = authors[1];
    final a3 = authors[2];
    final authorsStr = "$a1, $a2, and $a3";

    return new RefreshIndicator(
        child: new ListView(
          children: <Widget>[
            new ListTile(
              title: new Text("View Active Officers / Administrators"),
              onTap: () async {
                await Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SelectUserView(
                            "View",
                            false,
                            new BackendManager()
                                .data
                                .people
                                .where(
                                    (CommServPerson p) => p.isPersonOfficer())
                                .toList()
                                  ..sort((p1, p2) =>
                                      p1.lastName.compareTo(p2.lastName)),
                            "officer",
                            "officers",
                            false)));
              },
            ),
            new Divider(),
            new ListTile(
                title: new Text("${BackendManager.APP_NAME}"),
                subtitle: new Text("Version ${BackendManager.APP_VERSION}")),
            new ListTile(
              title: new Text("ServeUp.dev@gmail.com"),
              subtitle: new Text("Email us suggestions or bugs!"),
              leading: new Icon(Icons.email),
              onTap: () async {
                final url = "mailto:ServeUp.dev@gmail.com";
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw Exception("bad");
                }
              },
            ),
            new ListTile(
              title: new Text("View our private policy"),
              onTap: () async {
                final url = "http://kgvdevelopment.org/privatepolicy";
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw Exception("bad");
                }
              },
            ),
 
            new Text("Created by $authorsStr.\nCopyright (c) $yr $authorsStr."),
          ],
          padding: new EdgeInsets.only(
              left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
        ),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }
}
