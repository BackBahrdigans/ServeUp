// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Data _$DataFromJson(Map<String, dynamic> json) {
  return new Data(
      events: (json['events'] as List)
          .map((e) => new CommServEvent.fromJson(e as Map<String, dynamic>))
          .toList(),
      people: (json['people'] as List)
          .map((e) => new CommServPerson.fromJson(e as Map<String, dynamic>))
          .toList());
}

abstract class _$DataSerializerMixin {
  List<CommServEvent> get events;
  List<CommServPerson> get people;
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'events': events, 'people': people};
}
