import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';

import 'BackendManager.dart';
import 'CommServPerson.dart';

enum Option { CONTACT }

class SelectUserView extends StatefulWidget {
  final String _choice;
  final bool _confirm;
  final List<CommServPerson> _people;
  final String _peopleTypeSingular;
  final String _peopleTypePlural;
  final bool _makeSelection;
  @override
  createState() => new SelectUserViewState(
        this._choice,
        this._confirm,
        this._people,
        this._peopleTypeSingular,
        this._peopleTypePlural,
        this._makeSelection,
      );
  SelectUserView(this._choice, this._confirm, this._people,
      this._peopleTypeSingular, this._peopleTypePlural, this._makeSelection,
      {Key key})
      : super(key: key);
}

class SelectUserViewState extends State<SelectUserView> {
  SearchBar searchBar;

  String _choice;
  bool _confirm;
  List<CommServPerson> _people;
  String _peopleTypeSingular;
  String _peopleTypePlural;
  bool _makeSelection;
  String _search;
  GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  SelectUserViewState(
    this._choice,
    this._confirm,
    this._people,
    this._peopleTypeSingular,
    this._peopleTypePlural,
    this._makeSelection,
  ) {
    searchBar = new SearchBar(
        buildDefaultAppBar: getAppBar,
        setState: setState,
        onChanged: (newtext) => setState(() => _search = newtext),
        onSubmitted: (newtext) => setState(() => _search = null),
        onClosed: () => setState(() => _search = null),
        inBar: true);
  }

  String _modFirstLetter(String str, bool cap) {
    if (cap) {
      return str.substring(0, 1).toUpperCase() + str.substring(1);
    } else {
      return str.substring(0, 1).toLowerCase() + str.substring(1);
    }
  }

  String _getChoiceName(bool cap) {
    String tmp;
    if (_choice == null) {
      tmp = "select";
    } else {
      tmp = _choice;
    }
    return _modFirstLetter(tmp, cap);
  }

  String get _appBarTitle => _makeSelection
      ? "${_getChoiceName(true)} a ${_modFirstLetter(_peopleTypeSingular, true)}"
      : _modFirstLetter(_peopleTypePlural, true);
  Widget getAppBar(BuildContext bc) => new AppBar(
        title: new Text(_appBarTitle),
        backgroundColor: Colors.lightBlue,
        actions: <Widget>[searchBar.getSearchAction(context)] +
            (_makeSelection
                ? <Widget>[]
                : <Widget>[
                    PopupMenuButton<Option>(
                      onSelected: (Option result) async {
                        switch (result) {
                          case Option.CONTACT:
                            new BackendManager().openEmail(_people);
                            break;
                        }
                      },
                      itemBuilder: (BuildContext context) =>
                          <PopupMenuEntry<Option>>[
                            new PopupMenuItem<Option>(
                              value: Option.CONTACT,
                              child: new Text(
                                  'Email ${_modFirstLetter(_peopleTypePlural, true)}'),
                            ),
                          ],
                    )
                  ]),
      );
  @override
  Widget build(BuildContext context) {
    new BackendManager()
        .setContext(_scaffoldKey, _refreshIndicatorKey, setState);
    return new Scaffold(
        key: _scaffoldKey,
        appBar: searchBar.build(context),
        body: _buildSelectUserView());
  }

  List<CommServPerson> getSearchedPeople() {
    if (_search == null) return _people;
    return _people
        .where((csp) => csp.name.toLowerCase().contains(_search.toLowerCase()))
        .toList();
  }

  int convertIndex(List<CommServPerson> filtered, int filteredIndex) {
    print("returning ${filtered[filteredIndex].name}");
    return _people
        .indexWhere((csp) => csp.email == filtered[filteredIndex].email);
  }

  Widget _buildSelectUserView() {
    final searched = getSearchedPeople();
    return new RefreshIndicator(
        child: new ListView.builder(
          itemBuilder: (BuildContext contextN, int index) {
            return new ListTile(
              title: new Text(
                searched[index].title,
              ),
              onTap: () async {
                if (!_makeSelection) return;
                if (_confirm) {
                  await showDialog(
                    context: contextN,
                    barrierDismissible: false,
                    builder: (newContext) {
                      return new AlertDialog(
                        content: new Text(
                            "Are you sure you want to ${_getChoiceName(false)} ${searched[index].name}?"),
                        actions: <Widget>[
                          new FlatButton(
                              child: Text('Cancel'),
                              onPressed: () {
                                Navigator.of(newContext).pop();
                              }),
                          new FlatButton(
                              child: new Text(_getChoiceName(true)),
                              onPressed: () {
                                final res = convertIndex(searched, index);
                                Navigator.of(newContext).pop(res);
                                Navigator.of(contextN).pop(res);
                                Navigator.of(context).pop(res);
                              }),
                        ],
                      );
                    },
                  );
                } else {
                  Navigator.of(context).pop(convertIndex(searched, index));
                }
              },
            );
          },
          padding: new EdgeInsets.only(
              left: 16.0, right: 16.0, top: 8.0, bottom: 8.0),
          itemCount: searched.length,
        ),
        key: _refreshIndicatorKey,
        onRefresh: new BackendManager().onRefresh);
  }
}
