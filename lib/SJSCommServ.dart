import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';

import 'BackendManager.dart';
import 'Calendar.dart';

class SJSCommServ extends StatelessWidget {
  SJSCommServ() {
    MapView.setApiKey(BackendManager.MAP_KEY);
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: BackendManager.APP_NAME,
      theme: new ThemeData(
        primaryColor: Colors.white,
      ),
      home: new Calendar(),
    );
  }
}
